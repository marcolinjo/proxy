
from fastapi.responses import HTMLResponse

async def not_found(request, exc):
    """
    Eigenes Exception Handling
    """
    return HTMLResponse(content="HAMANET", status_code=exc.status_code)
