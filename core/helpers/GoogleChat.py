"""
Diese Modul kann verwendet werden um User über Sachverhalte zu informieren.
Es soll aus FastApi angesprochen werden und einzeln verwendet werden können.
https://developers.google.com/chat/api/reference/rest/v1/cards
"""

import json
import logging
import os
import sys
import requests
from dotenv import load_dotenv

load_dotenv()
CHAT_HOOKS = os.getenv("chat_hooks").split(",")
assert CHAT_HOOKS != None
logger = logging.getLogger()
#logger.setLevel(10)


def __build_header__(title, subtitle):
    """
    Baut den Headder der Message.

    :param title: Zählername
    :param subtitle: Konsequenz für den Kundenberater/Kunden
    :return:
    """
    header = {
        "title": title,
        "subtitle": subtitle,
        "imageUrl":"https://cdn-icons-png.flaticon.com/512/463/463612.png",
        "imageType": "CIRCLE",
        "imageAltText": "Bildfehlermeldung",
    }
    return header

def __build_section__(fehlerursache):
    """
    Fügt die Fehlerursache der Message hinzu.
    :param fehlerursache: Fehlerbeschreibung aus SAP
    :return:
    """

    message = {
        "decoratedText": {
            "startIcon": {
                "iconUrl":
                    "https://img.icons8.com/material-sharp/512/text.png"
            },
            "text": fehlerursache,
            "wrapText": True
        },
    }

    section = [
        {
            "header": "Ursache",
            "collapsible": False,
            "uncollapsibleWidgetsCount": 0,
            "widgets": [
                message
            ],
        },
    ]

    return section

def __build_message__(header, section):
    """
    Verbindet headder und section.
    :param header:
    :param section:
    :return:
    """
    body = {
        "cardsV2": [
            {
                "cardId": "unique-card-id",
                "card": {
                    "header": header,
                    "sections": section,
                },
            }
        ],
    }

    return body

def __send_message__(message):
    """
    Sendet die Error-Message an die Webhooks.
    """

    message_headers = {'Content-Type': 'application/json; charset=UTF-8'}
    
    for hook in CHAT_HOOKS:

        try:

            response = requests.post(
                url=hook
                , data=json.dumps(message)
                , headers=message_headers
            )

            logging.info("Anwender wurde informiert.")
            logging.debug("{}".format(str(response)))

        except:
            logging.error("Chat wurde nicht erreicht.")
            logging.error(sys.exc_info())

def send_error(handlung, referenz, grund):
    """
    Sendet die Fehlermeldung an denServermonitorChat.
    Kundenberater soll die Infos des Kunden bekommen.

    -Titel soll Zählernummer sein.
    -Subtitel soll Konsequenz für Kunden sein.
    -Beschreibung soll SAP-Fehlermeldung sein.

    :param title:
    :param message:
    :param status_code:
    :return:
    """

    #todo bild prüfen
    header = __build_header__(
        title=handlung
        ,subtitle=referenz
    )

    section = __build_section__(grund)
    body = __build_message__(header, section)

    __send_message__(body)


if __name__ == "__main__":

    send_error(
        handlung="Bitte Kunde anrufen."
        ,referenz= "Tel: 0531/383 0000"
        ,grund= "Er hatte langeweile."
    )
