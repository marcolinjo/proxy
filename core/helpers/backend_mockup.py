from fastapi import FastAPI
from fastapi.exceptions import *

from app.shemata import *

app = FastAPI(
    title="ISU Mockup",
    description="Mockup für die Entpunkte, die wir im ISU brauchen."
)

@app.get("/ableseaufträge", tags=["zähler"], 
    responses={
        "200": {"model": Todos},#io
        "401": {"model": BaseModel}, #kombination aus zähler und vk stimmt nicht.
        #"204": {"model": BaseModel}, #keine abrechnungsbereiten zähler gefunden.
        "503": {"model": BaseModel}, #server ist nicht erreichbar.
        }
)
async def get_ableseaufträge(karte: Legitimation):
    """
    Liefert die Ablesebereiten Zähler zum Vertragskonto.
    """
    return {}

@app.get("/allezähler", tags=["zähler"],
    responses= {
    "200": {"model": Todos},#io
    "401": {"model":BaseModel}, #kombination aus zähler und vk stimmt nicht.
    #"204": {"model": BaseModel}, #keine abrechnungsbereiten zähler gefunden.
    "503": {"model": BaseModel}, #server ist nicht erreichbar.
    } 
)
async def get_zählerzuvk(karte: Legitimation):
    """
    Liefert alle Zähler zum Vertragskonto.
    """
    return {}

@app.post("/zählerstandsmeldung", tags=["zähler"], 
    responses= {
    "201": {"model": BaseModel}, #io
    "401": {"model": BaseModel}, #kombination aus zähler und vk stimmt nicht.
    #kommentierung für unplausible notwendig
    #formatierung passt nicht
    "503": {"model": BaseModel}, #server ist nicht erreichbar.
    }
)
async def zählerstandsmeldung_annehmen(meldung: Zählerstandsmeldung):
    """
    Nimmt den Zählerstand entgegen.
    """
    return {}