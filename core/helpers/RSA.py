"""
Service, der Strings ver- und entschlüsselt.
Beim Laden sollten die Keys initialisiert werden.
Gegenwärtig wird noch bei jedem Zugriff der Public und der Privatkey aus dem Ordner geladen. Das geht sicher performanter z.B. durch den Cash.
"""

import rsa
import logging, sys, os

from dotenv import load_dotenv

ablage = os.getenv("secrets_folder", "../../secrets/")

#die keylänge sind immer vielfache von 2
#wenn eine message nicht verschlüssel werden kann, muss der key erhöht werden
#die keys müssen dann aber auch neu generiert werden
key_länge = 512*2

#loglevel = 10 #https://docs.python.org/3/howto/logging.html#logging-levels
#logging.getLogger().setLevel(loglevel)

def __holeKeysAusOrdner__():
    """
    Läd die Key aus dem Ordner.
    """
    logging.info("lade keys aus: {}".format(ablage))

    try:

        with open( ablage + "public_key.pem", "r") as f:
            key_data = f.read()
            public_key = rsa.PublicKey.load_pkcs1(keyfile=key_data)

        with open( ablage + "privat_key.pem", "r") as f:
            key_data = f.read()
            privat_key = rsa.PrivateKey.load_pkcs1(keyfile=key_data)
    
        assert public_key != None and privat_key != None
        return (public_key, privat_key)
    
    except:

        logging.error("Die Keys konnten nicht aus dem Ordner: {} geladen werden.".format(ablage))
        logging.error("mit 'python3 {} --generieren' kannst Du neue Keys generieren.".format(__file__))

try:   

    public_key, privat_key = __holeKeysAusOrdner__()

except:
    logging.warning("Keys wurden nicht gefunden. Bitte generiere Neue!")

def generiereSchlüsselpaar():
    """
    Generiert die Schlüssel und legt sie in den Ordner ablage
    """
    
    logging.warning("generiere Schlüssel(Keylänge:{}) nach: {}".format(key_länge,ablage))
    public_key, privat_key = rsa.newkeys(key_länge)
    
    def store(filename, key):

        with open( ablage + filename+ ".pem", "wb") as binary_file:
            binary_file.write(key)

    store(key=public_key.save_pkcs1(), filename="public_key")
    store(key=privat_key.save_pkcs1(), filename="privat_key")
    
    return (public_key, privat_key)

def verschlüssleString(message):
    """
    Läd die Schlüssel aus der Ablage.
    Verschlüssel den String mit Public_Key.
    Gibt den verschlüsselten String zurück.
    """
    logging.info("verschlüssle: {}".format(message))

    #public_key, privat_key = __holeKeysAusOrdner__()

    # String in Bytes umwandeln
    message_bytes = message.encode('utf8')

    # String mit öffentlichem Schlüssel verschlüsseln
    crypto = rsa.encrypt(message_bytes, pub_key=public_key)

    # Verschlüsselten String in Base64 kodieren, um ihn als Text darzustellen
    crypto_b64 = crypto.hex()

    logging.debug("Verschlüsselter String:{}".format(crypto_b64))
    logging.warning("keys: {}".format((public_key, privat_key)))
    return crypto_b64

def entschlüssleString(buchstabensalat):
    """
    Entschlüsselt den String mit dem Privatkey aus der Ablage
    """
    logging.info("entschlüssle: {}".format(buchstabensalat))

    #public_key, privat_key = __holeKeysAusOrdner__()

    # String in Bytes umwandeln
    buchstabensalat_bytes = bytes.fromhex(buchstabensalat)

    # String mit öffentlichem Schlüssel verschlüsseln
    message = rsa.decrypt(crypto=buchstabensalat_bytes, priv_key=privat_key)

    logging.debug("Verschlüsselter String:{}".format(message))
    return message.decode("utf-8")

if __name__ == "__main__":
    """
    Manueller Einstieg in den Service.
    Sollte das Generieren neuer Keys ermöglichen.
    """

    if(len(sys.argv)>1 and sys.argv[1] == "--generieren"):
        y = input("\nSollen die aktuellen Schlüssel in {} wirklich überschrieben werden? [Y]".format(ablage))
        if (y == "Y"):
            generiereSchlüsselpaar()
    
    #salat = verschlüssleString(message="kuckuck")
    #message = entschlüssleString(salat)
    #print(message, type(message))