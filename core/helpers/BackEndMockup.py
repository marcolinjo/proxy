"""
Das Modul kommuniziert mit dem geschützten Backend.
Es soll aus FastApi angesprochen werden und einzeln verwendet werden können.
Die Dokumentation des Endpunkte ist hier: https://api.sap.com/api/UT_ERP_ISU_UMC_0001/resource/Devices
"""

import logging, sys, os
from dotenv import load_dotenv
import requests 

load_dotenv()


def __init__():
    pass

def __getZutritt__():
    pass

def getAblesebereiteZähler(vertragskonto):
    """
    Holt die zum ablesebereiten Zähler (mit Zählwerken) der Vertragskontos aus dem Backend.
    :param: braucht dafür eine vertragskonto 
    :param: und ggf. einen zähler
    """
    return {
        "vertragskonto": "1234567890",
        "zählerliste": [
            {
            "bezeichnung": "2323232",
            "zählwerke": [
                {
                "id": "001",
                "obiscode": "122.232.1.",
                "stand": "1.0"
                },
                {
                "id": "002",
                "obiscode": "132.232.2.",
                "stand": "2.0"
                }
            ],
            "sparte": "strom",
            "ablesedatum_min": "01.01.2023",
            "ablesedatum_max": "31.12.2023"
            },
            {
            "bezeichnung": "dffvw123",
            "zählwerke": [
                {
                "id": "001",
                "obiscode": "132.232.1.",
                "stand": "1.0"
                }
            ],
            "sparte": "gas",
            "ablesedatum_min": "01.01.2023",
            "ablesedatum_max": "31.12.2023"
            }
        ],
        "ablesegrund": "02"
    }

    
    try:

        r = requests.post(endpunkt_eins, json={})
        return None
    
    except Exception as e:

        logging.error("Fehler beim Holen der Zählerstruktur.{}".format())
        logging.error("{}".format(e))
        logging.error("{}".format(sys.exc_info()))
        return None

def getAlleZähler(vertragskonto):
    """
    Holt ALLE Zähler (mit Zählwerken) der Vertragskontos aus dem Backend.
    """

    return {
        "vertragskonto": "1234567890",
        "zählerliste": [
            {
            "bezeichnung": "23232321232",
            "zählwerke": [
                {
                "id": "001",
                "obiscode": "122.232.1.",
                "stand": "1.0"
                },
                {
                "id": "002",
                "obiscode": "132.232.2.",
                "stand": "2.0"
                }
            ],
            "sparte": "strom",
            #"ablesedatum_min": "01.01.2023",
            #"ablesedatum_max": "31.12.2023"
            },
            {
            "bezeichnung": "dffvw123",
            "zählwerke": [
                {
                "id": "001",
                "obiscode": "122.232.1.",
                "stand": "1.0"
                }
            ],
            "sparte": "gas",
            #"ablesedatum_min": "01.01.2023",
            #"ablesedatum_max": "31.12.2023"
            },
            {
            "bezeichnung": "dedfefef",
            "zählwerke": [
                {
                "id": "001",
                "obiscode": "122.232.1.",
                "stand": "1.0"
                }
            ],
            "sparte": "wasser",
            #"ablesedatum_min": "01.01.2023",
            #"ablesedatum_max": "31.12.2023"
            }
        ],
        "ablesegrund": "02"
    }

    
    try:

        r = requests.post(endpunkt_eins, json={})
        return None
    
    except Exception as e:

        logging.error("Fehler beim Holen der Zählerstruktur.{}".format())
        logging.error("{}".format(e))
        logging.error("{}".format(sys.exc_info()))
        return None

def sendeZählerstand(zählerinfos):
    """
    Nimmt Zählerstand von Proxy entgegen.
    Formt den ggf. um.
    Leitet ihn ans Backend weiter.
    Sendet Anwort zurück an Proxy.
    """
    logging.info("zählerstand: bekommen.".format(str(zählerinfos)))

    #holt sich zutritt
    __getZutritt__()

    #reicht den zählerstand ein

    try:
        
        #erfolgreich abgegeben
        if(True):

            return {
                "status": "erfolgreich",
                "beschreibung": "zählerstand wurd notiert.",
                "zählerbezeichung": "dummydummdumm",
                "vertragskonto": "dummydummy"
            }

        #schon mal abgegeben

        #ablesedatum korrigieren
        elif(False):

            return {
                "status": "nacharbeiten"
                ,"was": {
                    "zählwerkid": "001"
                    ,"obiscode": "12323"
                    ,"ablesedatum": "123232323.223"
                }
                ,"bemerkung": "dummy"
            }

        #zählerstand korrigieren
        elif(False):

            return {
                "status": "nacharbeiten"
                ,"was": {
                    "zählwerkid": "001"
                    ,"obiscode": "12323"
                    ,"stand": "123232323.223"
                }
                ,"bemerkung": "dummy"
            }
        
        #später nochmal versuchen

        #kundenservice kontaktieren
        else:
            return None
        
    except Exception as e:

        logging.error("Fehler bei einreichen des Zählerstands an Backend.")

    
if __name__ == "__main__":
    """
    Manueller Einstieg in den Service
    """
    pass