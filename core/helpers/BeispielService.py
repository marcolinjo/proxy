"""
Diese Modul kann verwendet werden um User über Sachverhalte zu informieren.
Es soll aus FastApi angesprochen werden und einzeln verwendet werden können.
"""
import logging

logger = logging.getLogger()

def __init__():
    pass

def serve():
    return

if __name__ == "__main__":
    """
    Manueller Einstieg in den Service
    """
    pass