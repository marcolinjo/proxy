"""
Dieser File beinhaltet die verwendeten Datamodells.
"""

from pydantic import BaseSettings, BaseModel, Field
from typing import List, Optional
from dotenv import load_dotenv
load_dotenv()

class Settings(BaseSettings):
    """
    ApplikationsSettings in welche die Enves eingelesen werden.
    Steht der gesammten App zur Verfügung.
    """
    
    #über die app
    app_name: str
    app_beschreibung: str
    modus: str
    static_folder: str

    #todo - commit hast in enves verpacken... die sind gerade nicht drin.
    #git rev-parse --short HEAD
    actual_commit: str #Repo(search_parent_directories=True).head.object.hexsha
    
    cors_origins: str

    admin_name: str
    admin_email: str

    #über die db
    redis_host: str
    redis_port: int
    redis_passwort: str
