"""
Service, der es erlaubt in die Redis-Datenbank zu schreiben.
Soll auch einzeln aufrufbar sein.
Soll einen Kapsel haben die übergeben wird an Fast-Api, hier das Modul.
Enthält die Funktionen, die jeweils selbst einen neue Verbindung herstellen.
"""
import sys, os, time
import redis, pickle, json
import datetime
import logging
from dotenv import load_dotenv


def __getConnection__():
    """
    Stellt die Verbindung zur Datenbank her.
    Gibt die Verbindung an die Funktionen.
    """
    load_dotenv()
    con = redis.Redis(
            host=os.getenv("redis_host"),
            port=os.getenv("redis_port"),
            password=os.getenv("redis_passwort")
    )

    #test Verbindung
    assert con.ping()
    return con

### Daten als Zeitreihen behandeln.

def speichereEintragTimed(key, wert):
    """
    Speichert den Eintrag Wert in der Liste Key mit einen aktuellen Timestamp.
    Timestamp wird als ersten 20 Stellen des Datetimetimestamps interpretiert.
    :params: key - Name des Keys
    :params: wert - Numerischer Wert des Eintrags
    """
    #erstellen des timestamps
    #yyyymmttddmmssnnnnnn
    #redis nimmt nur die ersten 20 stellen
    #timestamp muss vom server kommmen, um kollisionen zu vermeiden.
    timestamp_d = str(datetime.datetime.now()).replace(" ", "").replace("-", "").replace(":", "").replace(".", "")[:19]
    #timestamp_t = time.time_ns()
    logging.debug("speichere eintrag in db: {}". format((timestamp_d, key, wert)))
    
    try:
        r = __getConnection__()

        reply = r.ts().add(
            key=key
            , timestamp=str(timestamp_d)
            , value=wert
        )
        logging.debug("datenbank antwortet: {}".format(reply))
        return reply

    except Exception as e:
        logging.exception("Fehler beim Speichern des Eintrags in Redis: {}".format(e))
        return 0
    
def liesEinträgeTimed(key, von, bis):
        """
        Liest die Einträge des Keys von VON bis BIS.
        Timestamp wird als ersten 20 Stellen des Datetimetimestamps interpretiert.
        :param: key - Name des Keys
        :param: von - Datum von wo an z.B. 20230424
        :param: bis - Datum bis wo hin z.B. 20230425
        :return: liste der results
        """

        r = __getConnection__()
        jetzt = str(datetime.datetime.now())[:19]
        von = von.ljust(19, '0')
        bis = bis.ljust(19, '0')

        logging.debug("hole Einträge aus DB mit key:{} von:{} bis:{}".format(key, von, bis))

        try:

            return r.ts().range(
                key=key
                ,from_time= von
                ,to_time=   bis
            )

        except Exception as e:
            logging.exception("Fehler beim Lesen des Keys: {} aus Redis: {}".format(key,e))
            return []

def getKeys(pattern):
    """
    Liefert alles Keys 
    :param: key - Name des Keys
    :return: 
    """

    logging.debug("{}".format((__doc__, pattern)))
    r = __getConnection__()

    reply = []

    try:

        reply = r.keys(
            pattern
        )

        return reply
    
    except Exception as e:
        logging.exception("Fehler beim Lesen des Keys: {} aus Redis: {}".format(pattern, e))
        return reply     

def getInfosTimed(key):
    """
    Liefert mehr Infos zu einem Key.
    :param: key - Name des Keys
    :return: 
    """

    r = __getConnection__()
    reply = {}

    try:

        reply["key"]=key
        tmp = r.ts().info(
            key=key
        )

        reply["länge"] = tmp.total_samples
        reply["von"] = tmp.first_timestamp
        reply["bis"] = tmp.last_timestamp
        return reply
    
    except Exception as e:
        logging.exception("Fehler beim Lesen der Infos: {} aus Redis: {}".format(key, e))
        return reply 

### Daten die nicht als Zeitreihen gespeichert werden.
def speichereEintrag(key, wert, dauer_in_tagen):
    """
    Speichert den Eintrag Wert in der Liste Key.

    :params: key - Name des Keys
    :params: wert - Numerischer Wert des Eintrags
    """
    logging.debug("speichere eintrag in db: {}". format((key, wert)))
    
    try:
        r = __getConnection__()

        reply = r.set(
            name=key
            ,value=wert
            ,ex=dauer_in_tagen*24*60*60
        )

        logging.debug("datenbank antwortet: {}".format(reply))
        return reply

    except Exception as e:
        logging.exception("Fehler beim Speichern des Eintrags in Redis: {}".format(e))
        return 0
 
def liesEintrag(key):
    """
    Holt den Eintrag aus der Datenbank, wenn vorhanden.

    :params: key - Name des Keys
    """
    logging.debug("hole eintrag in db: {}". format((key)))

    
    try:
    
        r = __getConnection__()

        reply = r.get(
            name=key
        )
        
        if(reply != None):
            reply = reply.decode('utf-8')

        logging.debug("datenbank antwortet: {}".format(reply))
        return reply

    except Exception as e:
        logging.exception("Fehler beim  Lesen des Eintrags in Redis: {}".format(e))
        return None
    
    #in jedem fall
    return None


if __name__ == "__main__":

    """
    Einstiegspunkt, um den Service direkt aufzurufen.
    Kann zum Beispiel zum Übertragen von Infos verwendet werden. 
    """

    reply = speichereEintragTimed(key="test", wert=1)
    reply = liesEinträgeTimed("test", "20210101", "20240101")
    reply = getKeys("*est*")
    reply = getInfosTimed("test")

    logging.warning(reply)