#startbar mit source ./build_and_run.sh

#setze die umgebungsvariable
export actual_commit="$(git rev-parse --short HEAD)"
echo "setze app version (actual_commit) auf: ${actual_commit}"

#starte building
echo "baue container proxy und starte ihn."
docker-compose up --build proxy