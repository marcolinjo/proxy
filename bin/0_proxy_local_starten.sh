#setze die umgebungsvariable
export actual_commit="$(git rev-parse --short HEAD)"
echo "setze app version (actual_commit) auf: ${actual_commit}"

#starte proxy local
echo "starte proxy local."
echo "achtung: dafür muss die datenbank local auf dem port (.env & docker compose) sein."
uvicorn app.server:app --reload