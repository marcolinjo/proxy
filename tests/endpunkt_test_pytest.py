"""
Ruft einen Endpunkt mit Testfällen auf und vergleicht das Ergebnis mit dem erwarteten.
HTTP STATUS CODES https://developer.mozilla.org/en-US/docs/Web/HTTP/Status#client_error_responses
"""

import requests

endpunkt = "https://www.google.de"
testcases = [
    {
        "input": "richtiger_datensatz"
        , "erwartetet_ergebnis": 405
    }
]

def teste_endpunkt():
    """
    Ruft den Endpunkt auf.
    Postet den Testdatensatz 0 gegen den Entpunkt.
    Vergleicht den Statuscode mit dem im Testsatz gespeicherten.
    Weicht der Wert ab, wird ein AssertionError geworfen.
    :return:
    """

    r = requests.post(endpunkt, json=testcases[0]["input"])
    assert r.status_code == testcases[0]["erwartetet_ergebnis"]


if __name__ == '__main__':
    teste_endpunkt()
