"""
Stellt die Verfügbaren Routes Online.
"""

from fastapi import APIRouter
from fastapi import FastAPI, Request, Body, Response, status
from fastapi.responses import PlainTextResponse, RedirectResponse
from fastapi.exceptions import HTTPException

from app.shemata import *
from app.services import *

router = APIRouter()

@router.get("/", tags=["alle"])
async def index():
    """
    Antwortet dem Kunden.
    ruft den Service auf
    """
    pass

