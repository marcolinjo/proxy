"""
Hier wird der Start des Servers beschrieben.
Und das app-Objekt wird initialisiert.
Die Umgebungsvariablen werden geladen.
CORS wird konfiguriert.
Das Logging wird konfiguriert.

Hier wird ein Server in Uvicorn und Fastapi bereit gestellt. https://fastapi.tiangolo.com/tutorial/first-steps/
Er wird gestartet via: 
    lokal: uvicorn server:app --reload
    dockered: source ./bin/proxy.sh

Der Proxy schützt das Backend.
Fehler sollen *handlungsrelevant* an Kunden und Kundenbetreuer werden!
"""

from fastapi import FastAPI, Request, Depends
from fastapi.staticfiles import StaticFiles
from fastapi.middleware import Middleware
from fastapi.middleware.cors import CORSMiddleware

from core.config import Settings
from api.router import router

import logging

def create_app() -> FastAPI:
    """
    Zentrale Funktion zum Start der Anwendung.
    Liest Config ein.
    Setzt CORS.
    Baut Middleware.
    """
    
    #config aus dotenv einlesen
    config = Settings()

    app_ = FastAPI(
        title=config.app_name +" (Version:"+ config.actual_commit +")",
        description=
            config.app_beschreibung 
            +" \nAnsprechpartner: " +config.admin_name
            +" ("+ config.admin_email +")",
        version=config.actual_commit
    )

    #Loglevel und docs setzen nach envs
    if config.modus != "prod" and config.modus != "production" : #wenn modus == dev
        logging.getLogger().setLevel(20)
        app_.redoc_url= "/redocs"
        app_.docs_url= "/docs"
    
    else: #loglevel = prod
        logging.getLogger().setLevel(40)
        app_.redoc_url = None
        app_.docs_url = None
    logging.info("Logging wurde auf Logging:{} eingerichtet.".format(logging.getLogger()))
    
    #CORS einrichten
    app_.add_middleware(
        CORSMiddleware,
        allow_origins=config.cors_origins.split(","),
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
    logging.info("CORS <{}> wurde eingerichtet.".format(str(config.cors_origins.split(","))))

    #static file serving einrichten
    app_.mount("/static", StaticFiles(directory=config.static_folder), name="static")
    logging.info("Fileserve von <{}> wurde eingerichtet.".format(config.static_folder))

    #router hinzufügen
    app_.include_router(router)

    return app_

app = create_app()

