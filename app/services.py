"""
Enthält die Implementierung der Services zu den Routes.
Die Services sind funktional geschrieben, da der Server zustandslos ist.
"""

import logging, random, string, datetime
from fastapi import FastAPI, Request, Body, Response, status
from fastapi.responses import PlainTextResponse, RedirectResponse
from fastapi.exceptions import HTTPException

from app.shemata import *
from core.db import Redis as red

from core.helpers import RSA as rsa
from core.helpers import GoogleChat as chat
from core.helpers import BackEndMockup as be

from core.config import Settings
settings = Settings()

def service_welcome(request: Request):
    """
    Zentraler Einstieg z.B. für Crawler.
    """
    logging.debug("request.{}".format(request, request.headers.keys()))
    
    #speichere user-agent für weitere auswertung
    user_agent = request.headers.get("user-agent")

    reply = red.speichereEintragTimed(
        key= settings.app_name +":user-agent:"+user_agent
        ,wert=1
    )

    return {
            "request_keys": request.headers.keys()
            ,"user-agent": user_agent
            ,"host": request.headers.get("host")
            ,"settings:": str(settings)
    }