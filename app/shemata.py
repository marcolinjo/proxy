"""
Dieser File beinhaltet die verwendeten Datamodells.
"""

from pydantic import BaseSettings, BaseModel, Field
from typing import List, Optional

class Anschreiben(BaseModel):
    """
    Anschreiben, welches im Rahmen des Abrechnungslaufes von Abrechnungssystem erzeugt wird.
    Gegenwärtig kämpfen wir mit hoher Druckverzögerung.
    Darum speichern wir zusätzlich das Druckdatum in die DB.
    """
    vertragskonto: str = Field(..., max_length=10, description="ID des Kunden bei uns")
    portion: str = Field(..., max_length=10)
    ableseeinheit: str = Field(..., max_length=10)
    gültig_dauer_tage: int

class Einladung(BaseModel):
    """
    Einladung, mit welcher sich der Kunde über das Frontend beim Proxy identifiziert.
    Link und Tan stammen aus dem persönlichen Anschreiben, welches während des Drucks erzeugt wurde.
    """
    link: str = Field(..., max_length=12)
    tan: str = Field(..., max_length=12)

class Legitimation(BaseModel):
    """
    Eintrittskarte, die der Kunde nach erfolgreichem Link-Tan-Vergleich erhält.
    Es sendet sie uns zurück, ob die Todos zu bekommen.
    Die Eintrittskarte wird sowohl während der Turnusablesung (mit persönlichem Anschreiben) und
    bei der Kontrollablesung (ohne Kundenanschreiben) verwendet.
    Im letzeren Fall identifiziert er sich mit VK und ZN.
    """
    vertragskonto: str = Field(..., max_length=12)
    zählerbezeichnung: str = Field(..., max_length=12)
    ablesegrund: str = Field(..., max_length=2, description="02-Turnus oder 09-Kontrollablesung")
    logging_goodies: Optional[str] = Field(..., max_length=100) 
    
class Zählwerk(BaseModel):
    """
    Das Zählwerk enthält den Zählerstand.
    """
    id: str = Field(..., max_length=3)
    obiscode: str = Field(..., max_length=10)
    stand: str = Field(..., max_length=12)

class Zähler(BaseModel):
    """
    Der Zähler enthält mehrere Zählwerke
    """
    bezeichnung: str = Field(..., max_length=12)
    zählwerke: List[Zählwerk]
    sparte: str = Field(..., max_length=10)
    ablesedatum_min: Optional[str] = Field(..., max_length=10, description="DD.MM.YYYY") 
    ablesedatum_max: Optional[str] = Field(..., max_length=10, description="DD.MM.YYYY")
   
class Todos(BaseModel):
    """
    Die Todos werden von uns an das Frontend übermittelt
    Sie enthält eine Liste von Zählern.
    Der Ablesegrund kann 02-Turnusablesung oder 09-Kontrollablesung sein.
    Für eine Kontrollablesung werden alle verfügbaren Zähler übermittelt.
    Für eine Turnusablesung werden die ablesbaren Zähler mit Ablesedatumsgültigkeiten übermittelt.
    """
    vertragskonto: str = Field(..., max_length=10)
    zählerliste: List[Zähler]
    ablesegrund: str = Field(...,max_length=2)

class Zählerstandsmeldung(BaseModel):
    """
    Die Zählerstandsmeldung wird an uns gesendet.
    Die Liste enthält immer einen Zähler.
    """
    vertragskonto: str = Field(..., max_length=10)
    zähler: Zähler
    ablesedatum: str = Field(..., max_length=10, description="DD.MM.YYYY")
    ablesegrund: str = Field(...,max_length=2)
    kommentar: Optional[str] = Field(None,max_length=100)

class Erfolgsmeldung(BaseModel):
    """
    Antwort des Servers auf Einreichnung des Zählerstands.
    """
    status: str = Field(..., max_length=3)
    beschreibung: str = Field(..., max_length=100)
    zählerbezeichung: str = Field(..., max_length=12)
    vertragskonto: str = Field(..., max_length=12)
    
class UserXP(BaseModel):
    """
    Step einer UserExperience, durch den Prozess.
    """
    von: str #schritt in app
    nach: str #schritt in app
    dauer: float #dauer dazwischen in sec

class Zeitfilter(BaseModel):
    """
    Filter für die Auswertung von UserExperiences aus Datenbank
    """
    von: str 
    bis: str
