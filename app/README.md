# App

Dieser Ordner enthält die Implementierung der Anwendung.

## Inhalte 
### server.py 

Ist das Startskript des Servers.

###  services.py

Enthält die Funktionen des Servers, welche in `api/routers.py` versprochen wurden.

### shemata.py

Enthält die Datenmodelle der Services, auf welche die Anwendung baut, und gegen welche die Daten der Api kontrolliert werden.