# Proxy

Vorlage für das Erstellen einen HTTPs-Python-Proxys der als Frontend eine JS-App bedient und im Backend einen Endpunkt schützt.

## Getting Startet

### 1. Mockup Für Frontend bereitstellen
* Für die Frontendentwicklung simulieren wir das Backend durch einen Proxy-Mockup.
* Hierfür ergänzen wir die Dummydaten in [Mockup](server_mockup_flask.py).
* Anschließend starten wir den Mockup und testen damit unser Frontend: `source bin/0_proxy_local_starten.sh`
  
### 2. Backend Testen
* Wir entwickeln unsere Anwendung gegen einen Endpunkt im Backend.
* Dafür überlegen wir uns mögliche Inputs die wir dem Backend senden, und dazu passende Outputs, die wir aus dem Backend erwarten. Beide tragen wir in den [Backend-Tester](./endpunkt_test_pytest.py) ein.
* Anschließend starten wir den Test:  `python3 tests/endpunkt_test_pytest.py`

### 3. Proxy bereitstellen
* Im letzen Schritt bauen wir den Proxy-Mockup zu einem funktionalen Proxy um.
* Anschließend stellen wir den Server via `source bin/0_proxy_docker_starten` lokal bereit.
* Den [Test](#2-backend-testen) sollten wir behalten, um auch später die Funktionalität des Backends prüfen zu können.
* Der Proxy kann via: `source bin/proxy.sh` gestartet werden.
* Das Backup kann via: `source bin/backup.sh` gestartet werden.
* Das Revocery kann via: `source bin/recovery.sh` gestartet werden.

## Inhalte

* Basiert auf FastApi
* Liefert des index.html, JS-Sourcen und CSS aus
* Bringt Anbindung an [Redis](https://redis.io/docs/getting-started/) mit
  * Backup der Datenbanken
  * Recovery der Datenbank
* Liefert [robots.txt](https://de.wikipedia.org/wiki/Robots_Exclusion_Standard) aus
* Steuert [CORS](https://fastapi.tiangolo.com/tutorial/cors/)
* Informiert User in GoogleChats
